import { Component, OnInit } from '@angular/core';

import { NavigationMainComponent } from '../navigation-main/navigation-main.component';

@Component({
  moduleId: module.id,
  selector: 'app-section-header',
  templateUrl: 'section-header.component.html',
  directives: [
    NavigationMainComponent
  ]
})
export class SectionHeaderComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
