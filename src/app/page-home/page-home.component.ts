import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-page-home',
  templateUrl: 'page-home.component.html',
  directives: [
      ROUTER_DIRECTIVES
  ]
})
export class PageHomeComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }



}
