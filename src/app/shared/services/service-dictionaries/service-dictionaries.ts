import {Injectable}    from '@angular/core';
import {Headers, Http} from '@angular/http';
import { Operator } from 'rxjs';

@Injectable()
export class DictionaryService {
    private url = {
        en: '/dictionaries/en.json'
        //de: '/dictionaries/en.json',
    };

    constructor(
        private http: Http
    ) { }

    getEnDictionary() {
        return this.http.request('./data/people.json')
            .map(res => res.json());
    }
}
