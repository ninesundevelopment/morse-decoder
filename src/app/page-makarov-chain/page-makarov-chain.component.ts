import {Component, OnInit} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-page-makarov-chain',
    templateUrl: 'page-makarov-chain.component.html',
    providers: []
})
export class PageMakarovChain implements OnInit {

    bCheckReady: boolean    = false;
    bMatrixReady: boolean   = false;
    bDownloadReady: boolean = false;
    bResultReady: boolean   = false;
    bResult: boolean        = false;


    planedFeaturesVisible: boolean = false;
    pageDescriptionVisible: boolean = true;

    inputMethod: number = 0;
    precision: number   = 30;

    inputText: string     = '';
    sanitizedText: string = '';

    outputJSON: string = '';

    markovMatrix                = {};
    letterMatrix: Array<string> = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    fileContent            = '';
    parsedFileContent: any = null;

    constructor() {
        // Build the Matrix
        this.markovMatrix = {};

        for (let i = 0; i < 26; i++) {
            let tmpMatrix = {};

            for (let ii = 0; ii < 26; ii++) {

                tmpMatrix[this.letterMatrix[ii]] = 0;

            }

            this.markovMatrix[this.letterMatrix[i]] = tmpMatrix;
        }
    }

    ngOnInit() {
    }


    togglePlanedFeatures(): void {
        this.planedFeaturesVisible = !this.planedFeaturesVisible;
    }

    togglePageDescription(): void {
        this.pageDescriptionVisible = !this.pageDescriptionVisible;
    }

    learnText(): void {
        this.bDownloadReady = false;

        this.sanitizeText();

        let words = this.sanitizedText.split(' ');

        for (let wordsIndex in words) {
            let currentWord = words[wordsIndex];

            for (let i = 0; i < currentWord.length - 1; i++) {
                this.markovMatrix[currentWord[i]][currentWord[i + 1]]++;
            }
        }

        for (let outerKey in this.markovMatrix) {
            let row    = this.markovMatrix[outerKey];
            let rowSum = 0;

            for (let innerKey in row) {
                if (row.hasOwnProperty(innerKey)) {
                    rowSum += row[innerKey];
                }
            }

            for (let innerKey in row) {
                if (row.hasOwnProperty(innerKey)) {
                    this.markovMatrix[outerKey][innerKey] = Math.log(row[innerKey] / rowSum);
                    this.markovMatrix[outerKey][innerKey] = isFinite(this.markovMatrix[outerKey][innerKey])
                        ? this.markovMatrix[outerKey][innerKey]
                        : 0;
                }
            }
        }

        console.log(this.markovMatrix);
        this.outputJSON = JSON.stringify(this.markovMatrix);
        console.log(JSON.parse(this.outputJSON));


        this.bDownloadReady = true;
    }

    sanitizeText(): void {
        this.sanitizedText = this.inputText.replace(new RegExp('[^a-zA-Z ]', 'g'), '').toLowerCase();
    }

    readFile(event): void {
        this.bResult      = false;
        this.bResultReady = false;
        this.bMatrixReady = false;
        if (event.target.files.length !== 0) {
            let reader       = new FileReader();
            reader.onloadend = () => {
                this.fileContent = reader.result;
                this.validateFile();
            };
            reader.readAsText(event.target.files[0]);
        } else {
            this.fileContent = '{}';
            this.validateFile();
        }
    }

    validateFile(): void {
        try {
            this.parsedFileContent = JSON.parse(this.fileContent);


            for (let letterKey in this.letterMatrix) {
                if (!this.parsedFileContent.hasOwnProperty(this.letterMatrix[letterKey])) {
                    throw '';
                } else {
                    for (let fileKey in this.parsedFileContent) {
                        if (!this.parsedFileContent[fileKey].hasOwnProperty(this.letterMatrix[letterKey])) {
                            throw '';
                        }
                    }
                }
            }

            this.bMatrixReady = true;
        } catch (e) {
            this.parsedFileContent = null;
            console.log("The selected file is not in a valid format.");
            return;
        }
    }

    checkText(): void {
        if (this.bMatrixReady && this.inputText !== '') {
            let word: string = '';
            let probability  = 0;
            let transition   = 0;


            this.sanitizeText();
            word = this.sanitizedText.replace(' ', '');

            for (let i = 0; i < word.length - 1; i++) {
                probability += this.parsedFileContent[word[i]][word[i + 1]];
                transition++;
            }

            this.bResult      = Math.exp(probability / Math.max(transition, 1)) <= (this.precision / 100);
            this.bResultReady = true;

        } else {
            alert("No Matrix-File or Text. Both are obviously needed.")
        }
    }

    safeProbabilityTable() {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.outputJSON));
        element.setAttribute('download', 'PropabilityTable-' + Date.now());

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

}
