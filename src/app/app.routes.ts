import {provideRouter, RouterConfig}  from '@angular/router';
import {PageHomeComponent} from './page-home/page-home.component';
import {PageDecoderComponent} from './page-decoder/page-decoder.component';
import {PageMakarovChain} from './page-makarov-chain/page-makarov-chain.component';

const routes: RouterConfig = [
    {
        path: '',
        component: PageHomeComponent
    },
    {
        path: 'home',
        component: PageHomeComponent
    },
    {
        path: 'decoder',
        component: PageDecoderComponent
    },
        {
            path: 'decoder/:code',
            component: PageDecoderComponent
        },
        {
            path: 'decoder/:code/:letterLength',
            component: PageDecoderComponent
        },
    {
        path: 'makarov-chain',
        component: PageMakarovChain
    }
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
