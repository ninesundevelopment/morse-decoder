import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Http, HTTP_PROVIDERS, Response} from '@angular/http';
//import {Operator} from 'rxjs';

@Component({
    moduleId: module.id,
    selector: 'app-page-decoder',
    templateUrl: 'page-decoder.component.html',
    providers: [
        HTTP_PROVIDERS
    ]
})
export class PageDecoderComponent implements OnInit {
    code: string = '';


    results: Array<any>             = [];
    resultsRaw: Array<any>          = [];
    enDict: Array<String>           = [];
    wbDict: Array<String>           = [];
    moDict: Array<any> = [];
    mcDict: Array<any> = [];
    pages: Array<number>            = [];
    possibleLengths: Array <number> = [1,5,10,25,50];

    bEnDictLoaded: boolean = false;
    bWbDictLoaded: boolean = false;
    bMoDictLoaded: boolean = false;
    bMcDictLoaded: boolean = false;

    bEnDictActivated: boolean = false;
    bWbDictActivated: boolean = false;
    bMcDictActivated: boolean = false;

    planedFeaturesVisible: boolean = false;
    pageDescriptionVisible: boolean = true;

    bSetPage = false;

    possibleMatches: number = 0;
    checkedMatches: number  = 0;
    letterLength: number    = 4;
    consumedTime: number    = 0;
    medianLetterTransition: number = 0;
    precision: number       = 0.3;

    pageIndex: number    = 0;
    maxPageIndex: number = 0;
    pageLength: number   = 25;
    wantedPage: number   = 1;

    ready = false;

    constructor(
        private route: ActivatedRoute,
        private http: Http
    ){
    }

    ngOnInit() {
        this.ready = false;

        this.route.params.forEach((params: Params) => {
            this.code         = params['code'];
            this.letterLength = +params['letterLength'];


            this.http.get('/dictionaries/en.json').map((res: Response) => res.json()).subscribe(res => {
                this.enDict        = res;
                this.bEnDictLoaded = true;
                this.validate();
            });

            this.http.get('/dictionaries/wb.json').map((res: Response) => res.json()).subscribe(res => {
                this.wbDict        = res;
                this.bWbDictLoaded = true;
                this.validate();
            });

            this.http.get('/dictionaries/mo.json').map((res: Response) => res.json()).subscribe(res => {
                this.moDict        = res;
                this.bMoDictLoaded = true;
                this.validate();
            });

            this.http.get('/dictionaries/mc.json').map((res: Response) => res.json()).subscribe(res => {
                this.mcDict        = res;
                this.bMcDictLoaded = true;
                this.validate();

                this.medianLetterTransition = 0;

                for(let outerKey in res) {
                    for(let innerKey in res[outerKey]){
                        this.medianLetterTransition += res[outerKey][innerKey];
                    }
                }

                this.medianLetterTransition = Math.exp(this.medianLetterTransition / 676);
            });
        });
    }

    toggleEnDictionary(): void {
        this.bEnDictActivated = !this.bEnDictActivated;
    }

    toggleWbDictionary(): void {
        this.bWbDictActivated = !this.bWbDictActivated;
    }

    toggleMcDictionary(): void {
        this.bMcDictActivated = !this.bMcDictActivated;
    }

    toggleDefinition(id): void {
        this.results[id].definitionVisible = !this.results[id].definitionVisible;
    }

    togglePlanedFeatures(): void {
        this.planedFeaturesVisible = !this.planedFeaturesVisible;
    }

    togglePageDescription(): void {
        this.pageDescriptionVisible = !this.pageDescriptionVisible;
    }

    pageUp():void {
        this.pageIndex = this.pageIndex < this.maxPageIndex ? this.pageIndex + 1 : this.pageIndex;
        this.wantedPage = this.pageIndex + 1;
        this.cropResults();
    }

    pageDown():void {
        this.pageIndex = this.pageIndex >= 1 ? this.pageIndex - 1 : this.pageIndex;
        this.wantedPage = this.pageIndex + 1;
        this.cropResults();
    }

    gotoPage(_pageIndex : number):void {
        if(_pageIndex <= this.maxPageIndex){
            this.pageIndex = _pageIndex;
            this.wantedPage = this.pageIndex + 1;
            this.cropResults();
        }
    }

    toggleSetPage() : void {
        this.bSetPage = !this.bSetPage;
    }

    checkPage(e) : void {
        if(e.keyCode && e.keyCode == 13){
            let pageToCheck = this.wantedPage -1;

            if(pageToCheck >= 0 && pageToCheck <= this.maxPageIndex){
                this.pageIndex = pageToCheck;
            }

            this.bSetPage = false;
            this.wantedPage = this.pageIndex + 1;
        }
    }

    calc(): void {
        if (!this.ready) return;

        const startTime = Date.now();

        this.wantedPage = this.pageIndex + 1;
        this.bSetPage   = false;
        this.pageIndex  = 0;

        this.resultsRaw = [];
        this.decode();

        this.possibleMatches = this.resultsRaw.length;

        this.filter();

        this.cropResults();

        this.consumedTime = Date.now() - startTime;
    }

    resetPageIndex() {
        this.pageIndex = 0;
    }

    cropResults() : void {
        this.pageLength = parseInt(this.pageLength.toString());
        if(this.pageLength <= this.resultsRaw.length){
            this.maxPageIndex = Math.floor(this.resultsRaw.length / this.pageLength);
        }else{
            this.maxPageIndex = 0;
        }

        if(this.pageIndex * this.pageLength <= this.resultsRaw.length){
            this.results = this.resultsRaw.slice((this.pageIndex * this.pageLength), (this.pageIndex * this.pageLength) + this.pageLength);
        }else{
            this.results = this.resultsRaw;
            this.pageIndex = 0;
        }
    }

    filter() : void{
        let  filteredList = this.resultsRaw;

        if (this.bEnDictActivated){
            filteredList = filteredList.filter(function (value) {
                let firstIndex = this.enDict.lengths[value.word.length-1] ? this.enDict.lengths[value.word.length-1] : 0;
                let lastIndex = this.enDict.lengths[value.word.length];

                for(let i = firstIndex; i < lastIndex; i++){
                    if(this.enDict.words[i].length > value.word.length) return false;
                    if(this.enDict.words[i] === value.word) return true;
                }

                return false;
            }, this);
        }

        if(this.bWbDictActivated){
            filteredList = filteredList.filter(function (value) {
                return this.wbDict[value.word.toUpperCase()] ? true : false;
            }, this);
        }

        for (let i in filteredList) {
            filteredList[i].definition        = this.wbDict[filteredList[i].word.toUpperCase()] || false;
            filteredList[i].definitionVisible = false;
        }

        this.resultsRaw = filteredList;
    }

    validate():void {
        this.ready = false;
        let codeReady = false;

        // Set boundaries
        if (!this.letterLength) this.letterLength = 4;
        if (this.letterLength < 1) this.letterLength = 1;
        if (this.letterLength > 5) this.letterLength = 5;

        // Set boundaries
        if(this.code){
            this.code = this.code.replace(/[^.-]/g, '');
            codeReady = true;
        }

        this.ready = (
            codeReady &&
            this.bEnDictLoaded &&
            this.bWbDictLoaded &&
            this.bMoDictLoaded &&
            (this.code.length !== 0)
        );
    }

    encode (str:string): string {
        let i, ii;
        let res = [];

        for (i = 0; i < str.length; i++) {
            for (ii = 0; ii < this.moDict.length; ii++) {
                if (str[i] === this.moDict[ii].s) {
                    res.push(this.moDict[ii].c);
                }
            }
        }

        return res.join(' ');
    }

    decode ( ) : void {
        let cache = {
            '0': ['']
        };
        for (var start = 0; start < this.code.length; start++) {

            for (var len = 1; len <= this.letterLength; len++) {

                if (start + len > this.code.length) {
                    continue;
                }

                if (!cache[start + len]) {
                    cache[start + len] = [];
                }

                var curCode = this.code.slice(start, start + len);

                for (let i in this.moDict) {
                    if (curCode == this.moDict[i].c){
                        for (var i_start = 0; i_start < cache[start].length; i_start++) {
                            let previous = cache[start][i_start];
                            if(this.bMcDictActivated && previous.length > 0) {
                                let poss = Math.exp(this.mcDict[previous[previous.length - 1]][this.moDict[i].s]);
                                let medi = this.medianLetterTransition;
                                let distance = (poss > medi) ? poss - medi : medi - poss;

                                if (distance > this.precision){
                                    break;
                                }
                            }
                            cache[start + len].push(cache[start][i_start] + this.moDict[i].s);
                        }
                    }
                }
            }
        }

        for(let i in cache[this.code.length]){
            this.resultsRaw.push({
                word: cache[this.code.length][i],
                code: this.encode(cache[this.code.length][i])
            });
        }


    };


    zeroPad (num, places) : any {
        const zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }


    setDefaultPrecision(event) {
        event.preventDefault();
        this.precision = 0.3;
    }

    setDefaultCode(event) {
        event.preventDefault();
        this.code = '';
    }

    setDefaultLetterLength(event) {
        event.preventDefault();
        this.letterLength = 4;
    }
}
