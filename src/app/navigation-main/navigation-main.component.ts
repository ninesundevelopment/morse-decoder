import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-navigation-main',
  templateUrl: 'navigation-main.component.html',
  directives: [
    ROUTER_DIRECTIVES
  ]
})
export class NavigationMainComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
