import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

import {SectionHeaderComponent} from './section-header/section-header.component';
import {SectionFooterComponent} from './section-footer/section-footer.component';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'app.component.html',
    directives: [
        ROUTER_DIRECTIVES,
        SectionHeaderComponent,
        SectionFooterComponent
    ]
})
export class AppComponent {
    title = 'app loaded...';
}
