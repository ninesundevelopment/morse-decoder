import { VipyFrontendPage } from './app.po';

describe('vipy-frontend App', function() {
  let page: VipyFrontendPage;

  beforeEach(() => {
    page = new VipyFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
