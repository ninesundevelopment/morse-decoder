export interface IMember {
    firstName : String,
    lastName  : String,
    gender    : Number, // 0 transgender, 1 female, 2 male
    imgURI?   : String,
    imgArr?   : String[],
    birthDay  : Number, // UTC Timestamp
    state     : Number  // 0 offline, 1 online within time period, 2 currently online 
}